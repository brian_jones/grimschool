from django.db import models
class person(models.Model):
	firstName = models.CharField(length = 40)
	surName = models.CharField(length = 50)
	
class teacher(person):
	deptSupervisor = models.ForeignKey(teacher)
	pastSupervisor = models.ForeignKey (teacher)

class student(person):
	name = models.OneToOneField(person)
	exitYear = models.IntField()
	tutor = models.ForeignKey(teacher)

class report(models.Model):
	student =  models.ForeignKey(student)
	teacher = models.ForeignKey(teacher)
	grade = models.CharField(length = 2)
	effort = models.IntField()