TornadoSchool
=============
[![Build Status](https://travis-ci.org/grimley517/GrimSchool.svg?branch=master)](https://travis-ci.org/grimley517/GrimSchool)

This is a School Management System based on the Tornado Framework which allows for Staff Management, Timetabling and resource allocation and student grouping.

This will be a Course management system based on serving from a tornado server using python and either django or flask.  Ideally It will use a noSQL datastore to reflect the amorpohous nature of data held in schools.
