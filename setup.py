from distutils.core import setup
setup(name='GrimSchool',
    version= '0.1',
    url='www.noneyet',
    description='Web Application',
    author= 'Brian Jones',
    author_email='ba.jones.me@gmail.com',
    packages=['grimschool'],
    package_dir = {'grimschool':'grimschool'}
    classifiers = [
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Web :: Google Applications',
        'Intended Audience :: Education',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.3'
    )
